import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerrosFormComponent } from './perros-form.component';

describe('PerrosFormComponent', () => {
  let component: PerrosFormComponent;
  let fixture: ComponentFixture<PerrosFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerrosFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerrosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
