import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Perro } from '../interfaces/perro';
import { ModalidadServices } from '../services/modalidad.service';

@Component({
  selector: 'perros-form',
  templateUrl: './perros-form.component.html',
  styleUrls: ['./perros-form.component.css']
})
export class PerrosFormComponent implements OnInit {

  newPerro!:Perro;
  perroAdded = false;
  @ViewChild('formPerro') formPerro:NgForm;


  constructor(
    private perroServices:ModalidadServices,
    private router: Router,
    private title: Title
    ) { }

  ngOnInit(): void {
    this.title.setTitle('Perretes | Añadir Perrete');
    this.resetForm();
  }

  addPerro():void{
    this.perroServices.addPerro(this.newPerro).subscribe(
      perro =>{
        this.perroAdded = true;
        this.router.navigate(['/perros']);
      },
      error => console.error(error)
    );
  }

  resetForm():void{
    this.newPerro={
      nombre:'',
      raza:'',
      categoria:null,
      url:''
    };
  }
}
