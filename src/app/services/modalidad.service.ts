import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Categoria } from '../interfaces/categoria';
import { Modalidad } from '../interfaces/modalidad';
import { Participa } from '../interfaces/participa';
import { Perro } from '../interfaces/perro';

@Injectable({
  providedIn: 'root',
})
export class ModalidadServices {
  private readonly URL_MODALIDAD = 'modalidad';
  private readonly URL_PERROS = 'perros';
  private readonly URL_PARTICIPA = 'participa';
  private readonly URL_CATEGORIAS = 'categorias';
  private readonly URL_PARTICIPACIONES_FILTRADAS_MODALIDAD =
    'participa/filtered-by-modalidad/';

  constructor(private http: HttpClient) {}

  getModalidades(): Observable<Modalidad[]> {
    return this.http.get<Modalidad[]>(this.URL_MODALIDAD);
  }

  getPerros(): Observable<Perro[]> {
    return this.http.get<Perro[]>(this.URL_PERROS);
  }

  getCategoria(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.URL_CATEGORIAS);
  }

  getParticipa(id: number): Observable<Participa[]> {
    return this.http.get<Participa[]>(this.URL_PARTICIPACIONES_FILTRADAS_MODALIDAD + id);
  }
  deletePerro(id: number): Observable<void> {
    return this.http.delete<void>(`${this.URL_PERROS}/${id}`);
  }

  addPerro(perro:Perro): Observable<Perro>{
    return this.http.post<Perro>(this.URL_PERROS,perro);
  }
  addCompetencia(participa:Participa): Observable<Participa>{
    console.log(participa);
    return this.http.post<Participa>(this.URL_PARTICIPA,participa);
  }
  }
