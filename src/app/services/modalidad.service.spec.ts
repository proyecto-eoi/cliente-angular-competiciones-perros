import { TestBed } from '@angular/core/testing';

import { ModalidadServices } from './modalidad.service';

describe('ModalidadServicesService', () => {
  let service: ModalidadServices;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalidadServices);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
