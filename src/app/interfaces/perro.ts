import { Categoria } from './categoria';

export interface Perro {
  id?: number;
  categoria: Categoria;
  nombre: string;
  raza: string;
  url: string;
}
