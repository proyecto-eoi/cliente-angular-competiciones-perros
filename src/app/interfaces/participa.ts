import { Categoria } from './categoria';
import { Modalidad } from './modalidad';
import { Perro } from './perro';

export interface Participa {
  id?: number;
  categoria: Categoria;
  modalidad: Modalidad;
  perros: Perro;
  fecha: Date;
}
