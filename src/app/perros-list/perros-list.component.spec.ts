import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerrosListComponent } from './perros-list.component';

describe('PerrosListComponent', () => {
  let component: PerrosListComponent;
  let fixture: ComponentFixture<PerrosListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerrosListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerrosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
