import { Component, OnInit } from '@angular/core';
import { Perro } from '../interfaces/perro';
import { ModalidadServices } from '../services/modalidad.service';

@Component({
  selector: 'perros-list',
  templateUrl: './perros-list.component.html',
  styleUrls: ['./perros-list.component.css'],
})
export class PerrosListComponent implements OnInit {
  title = 'Perros';

  header = {
    categoria: 'Categoria',
    nombre: 'Nombre',
    raza: 'Raza',
    url: 'Imgane',
  };

  perros: Perro[] = [];

  constructor(private perrosServices: ModalidadServices) {}

  ngOnInit(): void {
    this.perrosServices.getPerros().subscribe(
      (perros) => (this.perros = perros)
      );
  }

  deleteProduct(perro: Perro): void {
    this.perros = this.perros.filter((p) => p !== perro);
  }
}
