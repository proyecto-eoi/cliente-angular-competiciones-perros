import { Component, OnInit } from '@angular/core';
import { Modalidad } from '../interfaces/modalidad';
import { ModalidadServices } from '../services/modalidad.service';

@Component({
  selector: 'modalidad-list',
  templateUrl: './modalidad-list.component.html',
  styleUrls: ['./modalidad-list.component.css'],
})
export class ModalidadListComponent implements OnInit {
  title = 'Modalidades';
  modalidades: Modalidad[] = [];

  constructor(private modalidadServices: ModalidadServices) {}

  ngOnInit(): void {
    this.modalidadServices.getModalidades().subscribe(
      (modalidades) => (this.modalidades = modalidades)
      );
  }
}
