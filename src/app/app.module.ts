import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ModalidadListComponent } from './modalidad-list/modalidad-list.component';

import { ModalidadItemComponent } from './modalidad-item/modalidad-item.component';
import { AppRoutingModule } from './app-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { PerrosListComponent } from './perros-list/perros-list.component';
import { PerrosItemComponent } from './perros-item/perros-item.component';
import { BaseUrlInterceptor } from './interceptors/base-url.interceptor';
import { PerrosFormComponent } from './perros-form/perros-form.component';


@NgModule({
  declarations: [
    AppComponent,
    ModalidadListComponent,
    ModalidadItemComponent,
    WelcomeComponent,
    PerrosListComponent,
    PerrosItemComponent,
    PerrosFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: BaseUrlInterceptor, multi: true, }],
  bootstrap: [AppComponent]
})
export class AppModule { }
