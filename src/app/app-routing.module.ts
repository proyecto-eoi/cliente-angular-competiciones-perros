import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModalidadListComponent } from './modalidad-list/modalidad-list.component';
import { PerrosFormComponent } from './perros-form/perros-form.component';
import { PerrosListComponent } from './perros-list/perros-list.component';
import { WelcomeComponent } from './welcome/welcome.component';

const ROUTES: Routes = [
  { path: 'modalidades', component: ModalidadListComponent },
  { path: 'perros', component: PerrosListComponent },
  { path: 'form', component: PerrosFormComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: '**', redirectTo: '/welcome', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
