import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerrosItemComponent } from './perros-item.component';

describe('PerrosItemComponent', () => {
  let component: PerrosItemComponent;
  let fixture: ComponentFixture<PerrosItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerrosItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerrosItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
