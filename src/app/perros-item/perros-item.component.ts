import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Modalidad } from '../interfaces/modalidad';
import { Participa } from '../interfaces/participa';
import { Perro } from '../interfaces/perro';
import { ModalidadServices } from '../services/modalidad.service';

@Component({
  selector: 'perros-item',
  templateUrl: './perros-item.component.html',
  styleUrls: ['./perros-item.component.css'],
})
export class PerrosItemComponent implements OnInit {
  @Input() perro: Perro;
  @Input() participa: Participa;
  @Output() deleted = new EventEmitter<void>();
  @Output() add = new EventEmitter<Perro>();
  @Output() addm = new EventEmitter<Modalidad>();
  private participaAdded = false;
   newCompetencia!:Participa;

  constructor(
    private perrosServices: ModalidadServices,
    private router: Router
    ) {}

  ngOnInit(): void {
    this.iniDatos();
  }

  deletePerros() {
    this.perrosServices.deletePerro(this.perro.id).subscribe(
      () => this.deleted.emit()
      );
  }

  addCompetencia():void{
    console.log(this.newCompetencia);
    this.perrosServices.addCompetencia(this.newCompetencia).subscribe(
      competencia => {
        this.participaAdded = true;
        this.router.navigate(['/modalidades']);
        this.add.emit(competencia.perros),
        this.addm.emit(competencia.modalidad)
      },
      error => console.log(error)
    );
  }
  iniDatos():void{
    this.newCompetencia={
      id:0,
      categoria:{
        id: 0,
        nombre: ''
      },
      modalidad: {
        id: 0,
        nombre: '',
        url: ''
      },
      perros:this.perro,
      fecha: new Date()
    };
  }
}
