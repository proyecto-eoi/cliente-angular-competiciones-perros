import { Component, Input, OnInit } from '@angular/core';
import { Categoria } from '../interfaces/categoria';
import { Modalidad } from '../interfaces/modalidad';
import { Participa } from '../interfaces/participa';
import { Perro } from '../interfaces/perro';
import { ModalidadServices } from '../services/modalidad.service';

@Component({
  selector: 'modalidad-item',
  templateUrl: './modalidad-item.component.html',
  styleUrls: ['./modalidad-item.component.css'],
})
export class ModalidadItemComponent implements OnInit {
  @Input() participa: Participa;
  participas: Participa[] = [];

  @Input() modalidad: Modalidad;

  perros: Perro[] = [];
  @Input() perro: Perro;

  categorias: Categoria[] = [];
  @Input() categoria: Categoria;

  constructor(private modalidadServices: ModalidadServices) {}

  ngOnInit(): void {
    this.modalidadServices.getCategoria().subscribe(
      (c) => (this.categorias = c)
      );

    this.modalidadServices.getPerros().subscribe(
      (p) => (this.perros = p)
      );

    this.modalidadServices.getParticipa(this.modalidad.id).subscribe(
      (p) => (this.participas = p)
      );
  }
}
