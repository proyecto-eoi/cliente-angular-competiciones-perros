import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalidadItemComponent } from './modalidad-item.component';

describe('ModalidadItemComponent', () => {
  let component: ModalidadItemComponent;
  let fixture: ComponentFixture<ModalidadItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalidadItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalidadItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
